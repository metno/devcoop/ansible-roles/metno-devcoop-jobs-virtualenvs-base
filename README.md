# Description

Ansible role to prepare for jobs and virtualenvs. This role should be
used as a dependency from other roles.

Positions for vertical profiles / cross-sections must be specified in
a file `{{ metws_job_scripts }}/util/vcross_positions.txt` which is
not part of this role.
